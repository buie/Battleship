import java.util.ArrayList;
public class Ship
{
    public static final int AIRCRAFT_CARRIER = 0;
    public static final int BATTLESHIP = 1;
    public static final int CRUISER = 2;
    public static final int DESTROYER = 3;
    public static final int SUBMARINE = 4;
    
    public static final int AIRCRAFT_CARRIER_LENGTH = 5;
    public static final int BATTLESHIP_LENGTH = 4;
    public static final int CRUISER_LENGTH = 3;
    public static final int DESTROYER_LENGTH = 2;
    public static final int SUBMARINE_LENGTH = 1;
    
    public static final String AIRCRAFT_CARRIER_STRING = "Aircraft Carrier";
    public static final String BATTLESHIP_STRING = "Battleship";
    public static final String CRUISER_STRING = "Cruiser";
    public static final String DESTROYER_STRING = "Destroyer";
    public static final String SUBMARINE_STRING = "Submarine";
    
    ArrayList<Location> mLocations;
    private int mShipName;
    int mShipType;
    
    public Ship(int shipType)
    {
        mLocations = new ArrayList<Location>();
        mShipType = shipType;
    }
    
    public int getShipType()
    {
        return mShipType;
    }
    
    public String getShipName()
    {
        switch (mShipType)
        {
            case AIRCRAFT_CARRIER: return AIRCRAFT_CARRIER_STRING;
            case BATTLESHIP: return BATTLESHIP_STRING;
            case CRUISER: return CRUISER_STRING;
            case DESTROYER: return DESTROYER_STRING;
            case SUBMARINE: return SUBMARINE_STRING;
            
            default: return "Unrecognized ship type.";
        }
    }
    
    public int getShipLength(int shipType)
    {
        int mShipType = shipType;
        switch (mShipType)
        {
            case AIRCRAFT_CARRIER: return AIRCRAFT_CARRIER_LENGTH;
            case BATTLESHIP: return BATTLESHIP_LENGTH;
            case CRUISER: return CRUISER_LENGTH;
            case DESTROYER: return DESTROYER_LENGTH;
            case SUBMARINE: return SUBMARINE_LENGTH;
            
            default: return 0;
        }
    }
    
    public ArrayList<Location> getLocation()
    {
        return mLocations;
    }
    
    public void setLocation(ArrayList<Location> locs)
    {
        mLocations = locs;
    }
}