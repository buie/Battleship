import javax.swing.JOptionPane;
public class Grid
{  
    private int mGrid[][];
    
    public Grid()
    {
        mGrid = new int[Location.MAX_ROWS][Location.MAX_COLUMNS];
        for (int i = 0; i < Location.MAX_ROWS; i++)
            for (int j = 0; j < Location.MAX_COLUMNS; j++)
                mGrid[i][j] = 0;
    }

    public Grid(int numRows, int numCols)
    {
        mGrid = new int[numRows][numCols];
        for (int i = 0; i < Location.MAX_ROWS; i++)
            for (int j = 0; j < Location.MAX_COLUMNS; j++)
                mGrid[i][j] = 0;
    }

    public String createGridString()
    {
        String retString = "";
        retString += "\t";
        // print out the column labels
        for (int i = 0; i < Location.MAX_COLUMNS; i++)
        {
            retString += Location.getColumnLabel(i) + "\t";
        }
        retString += "\n";
        // print out the rows
        for (int i = 0; i < Location.MAX_ROWS; i++)
        {
            retString += Location.getRowLabel(i) + "\t";
            for (int j = 0; j < Location.MAX_COLUMNS; j++)
            {    
                if (mGrid[i][j] == 0)
                {
                    retString += "[ ]\t";
                }
                else if(mGrid[i][j] > 0)
                {
                    retString += "X\t";
                }
                else if(mGrid[i][j] < 0)
                {
                    retString += "O\t";
                }
            }
            retString += "\n";
        }
        return retString;
    }
    
    public void setLocationState(Location theLoc, boolean state)
    {
        if(state == true)
                mGrid[theLoc.getXCoord()][theLoc.getYCoord()] = 1;
        else if(state == false)
                if(mGrid[theLoc.getXCoord()][theLoc.getYCoord()] > 0)
                {
                    JOptionPane.showMessageDialog(null, "Location already hit!");
                }
                else
                    mGrid[theLoc.getXCoord()][theLoc.getYCoord()] = -1;
    }
}