public class Location
{
    public static final int NORTH = 0;
    public static final int SOUTH = 1;
    public static final int EAST = 2;
    public static final int WEST = 3;
    
    public static final int MAX_COLUMNS = 10;
    public static final int MAX_ROWS = 10;
    
    private int mXCoord;
    private int mYCoord;
    
    //turn valid location into coordinates
    Location(int xCoord, int yCoord)
    {
        mXCoord = xCoord;
        mYCoord = yCoord;
    }
    
    public static Location getRandomLocation()
    {
        int xCoord = (int) (Math.random() * MAX_COLUMNS);
        int yCoord = (int) (Math.random() * MAX_ROWS);
        
        Location newLocation = new Location(xCoord, yCoord);
        return newLocation;
    }
    
    public int getXCoord()
    {
        return mXCoord;
    }
    
    public int getYCoord()
    {
        return mYCoord;
    }
    
    public static int getRandomDirection()
    {
        return (int)(Math.random() * 4);
    }
    
    public static String getColumnLabel(int i)
    {
        String mColumn = Integer.toString(i);
        return mColumn;
    }
    
    public static String getRowLabel(int i)
    {
        String mString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char mCRow = mString.charAt(i);
        String mSRow = Character.toString(mCRow);
        return mSRow;
    }
    
    public boolean checkFit(int length, int direction)
    {
        if(direction == NORTH)
        {
            if((mYCoord - length) < 0)
                return false;
        }
        else if(direction == SOUTH)
        {
            if((length + mYCoord) > MAX_ROWS - 1)
                return false;
        }
        else if(direction == EAST)
        {
            if((length + mXCoord) > MAX_COLUMNS - 1)
                return false;
        }
        else if(direction == WEST)
        {
            if((mXCoord - length) < 0)
                return false;
        }
        return true;
    }
    
    public Location getNeighboringLoction(int direction)
    {
        int mY = mYCoord;
        int mX = mXCoord;
        
        if (direction == 0) //North
        {
             mY--;
        }
        else if (direction == 1) //South
        {
             mY++;
        }
        else if (direction == 2) //East
        {
             mX++;
        }
        else if (direction == 3) //West
        {
             mX--;
        }
        Location mNeighboringLoc = new Location(mX, mY);
        return mNeighboringLoc;
    }
    
    public boolean checkEquals(Location rhs)
    {
        if (mXCoord == rhs.mXCoord && mYCoord == rhs.mYCoord)
            return true;
        else
            return false;
    }
    
    public String toString()
    {
        String xChar = "";
        if(mXCoord == 0)
            xChar = "A";
        else if(mXCoord == 1)
            xChar = "B";
        else if(mXCoord == 2)
            xChar = "C";
        else if(mXCoord == 3)
            xChar = "D";
        else if(mXCoord == 4)
            xChar = "E";
        else if(mXCoord == 5)
            xChar = "F";
        else if(mXCoord == 6)
            xChar = "G";
        else if(mXCoord == 7)
            xChar = "H";
        else if(mXCoord == 8)
            xChar = "I";
        else if(mXCoord == 9)
            xChar = "J";
        Integer.toString(mYCoord);
        String retString = xChar + Integer.toString(mYCoord); 
        return retString;
    }
}