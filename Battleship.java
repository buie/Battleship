import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.awt.TextArea;
import javax.swing.JTextArea;
public class Battleship
{
    int numGuesses;
    ArrayList<Ship> mShipList;
    Grid mGrid = new Grid();
    String input = "";
    public Battleship()
    {
        mShipList = new ArrayList<Ship>();
        mGrid = new Grid(Location.MAX_ROWS, Location.MAX_COLUMNS);
        placeShip(new Ship(Ship.AIRCRAFT_CARRIER));
        placeShip(new Ship(Ship.BATTLESHIP));
        placeShip(new Ship(Ship.CRUISER));
        placeShip(new Ship(Ship.DESTROYER));
        placeShip(new Ship(Ship.DESTROYER));
        placeShip(new Ship(Ship.SUBMARINE));
        placeShip(new Ship(Ship.SUBMARINE));
    }

    public void play()
    {
        boolean gDone = false;
        //printShips();
        int choice2 = JOptionPane.showConfirmDialog(null, "Do you want to see a printed list of ship locations?\n(For Developer Use)", "Battleship", JOptionPane.YES_NO_CANCEL_OPTION);
            if(choice2 == JOptionPane.YES_OPTION)
            {
                printShips();
            }
        while(gDone != true)
        {
            String retString = mGrid.createGridString();
            JTextArea text = new JTextArea(1,1);
            text.setText(retString  + "\n\tGuess a location to bomb. (Press \"Cancel\" to quit)\t\t");
            text.setTabSize(3);
            text.setEditable(false);
            input = JOptionPane.showInputDialog(null, text, "BattleShip", JOptionPane.PLAIN_MESSAGE);
            
            if(input != null)
            {
                if(checkValidLocation(input) == true)
                    processGuess(input);
                else
                    JOptionPane.showMessageDialog(null, "Invalid location! Try again");
            }
            else 
                    gDone = true;
            if(mShipList.isEmpty())
            {
                gDone = true;
                JOptionPane.showMessageDialog(null, "Congratulations!\n You sunk all of the enemy Ships!");
            }
        }
    }
    
    public boolean processGuess(String input)
    {
        int xCoord = 0;
        char xChar = input.charAt(0);
        char xCharUp = Character.toUpperCase(xChar);
        if(xCharUp == 'A')
            xCoord = 0;
        else if (xCharUp == 'B')
            xCoord = 1;
        else if (xCharUp == 'C')
            xCoord = 2;
        else if (xCharUp == 'D')
            xCoord = 3;
        else if (xCharUp == 'E')
            xCoord = 4;
        else if (xCharUp == 'F')
            xCoord = 5;
        else if (xCharUp == 'G')
            xCoord = 6;
        else if (xCharUp == 'H')
            xCoord = 7;
        else if (xCharUp == 'I')
            xCoord = 8;
        else if (xCharUp == 'J')
            xCoord = 9;
        
        char yChar = input.charAt(1);
        int yCoord = Character.getNumericValue(yChar);
        Location theLoc = new Location(xCoord, yCoord);
        
        for (Ship ship : mShipList)
        {
            for (Location loc : ship.getLocation())
            {
                if(loc.checkEquals(theLoc) == true)
                {
                    //System.out.println("===>Hit<===");
                    mGrid.setLocationState(theLoc, true);
                    ship.getLocation().remove(loc);
                    if(ship.getLocation().isEmpty())
                    {
                        mShipList.remove(ship);
                        JOptionPane.showMessageDialog(null, "Congratulations!\n You sunk a Ship!");
                    }    
                    return true;
                }   
            }            
        }
        mGrid.setLocationState(theLoc, false);
        return false;
    }
    
    private void placeShip(Ship newShip)
    {
        ArrayList<Location> temp = new ArrayList<Location>();
        boolean placed;
        do
        {
            placed = true;
            Location nextLoc = Location.getRandomLocation();
            if ((checkOccupied(nextLoc)) == true)
                placed = false;
            else
            {
                temp.add(nextLoc);
                int direction = Location.getRandomDirection();
                if (nextLoc.checkFit(newShip.getShipLength(newShip.getShipType()), direction) == false)
                    {
                    placed = false;
                    temp.clear();
                    }
                else
                    for(int i = 1; i < newShip.getShipLength(newShip.getShipType()); i++)
                    {
                        nextLoc = nextLoc.getNeighboringLoction(direction);
                        if(checkOccupied(nextLoc) == true)
                        {
                           placed = false;
                           temp.clear();
                           break;
                        }          
                        else
                            temp.add(nextLoc);
                    }
                }
        }while(placed != true);
        newShip.setLocation(temp);
        mShipList.add(newShip);
    }
    
    public static boolean checkValidLocation(String locString)
    {
        if(locString.length() > 2)
        {
            return false;
        }
        locString = locString.toUpperCase();
        char alpha = locString.charAt(0);
        if(alpha < 'A' || alpha > 'J')
        {
            return false;
        }
        int num = Character.getNumericValue(locString.charAt(1));
        if(num < 0 || num > 9)
        {
            return false;
        }
        return true;
    }
    
    public boolean checkOccupied(Location checkLocation)
    {
        for (Ship ship : mShipList)
        {
            for (Location loc : ship.getLocation())
            {
                if(loc.checkEquals(checkLocation) == true)
                    return true;
            }
        }
        return false;
    }
    
    public void printShips()
    {
        for (Ship ship : mShipList)
        {
            System.out.print(ship.getShipName() + ": ");
            for (Location loc : ship.getLocation())
            {
                System.out.print(loc.toString() + " ");
            }
            System.out.println("");
        }
    }
}
