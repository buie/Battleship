import javax.swing.JOptionPane;
public class Driver
{
    public static void main(String[] args)
    {
       boolean done = false;
       do
       {
           int choice = JOptionPane.showConfirmDialog(null, "Ready to test your skills in battleship?", "Battleship", JOptionPane.YES_NO_CANCEL_OPTION);
           if(choice == JOptionPane.NO_OPTION || choice == JOptionPane.CANCEL_OPTION)
           {
               done = true;
           }
           else
           {
               int choice1 = JOptionPane.showConfirmDialog(null, "Do you want to view the instructions before starting?", "Battleship", JOptionPane.YES_NO_CANCEL_OPTION);
               
               if(choice1 == JOptionPane.YES_OPTION)
               {
                   JOptionPane.showMessageDialog(null, "The object of the game is to sink all of the enemy ships. \nThere is 1 Aircraft Carrier (5 spaces), 1 Battleship (4 spaces), \n1 Cruiser (3 spaces), 2 Destroyers (2 spaces), and 2 Submarines (1 space). \nWithout knowing the location of these ships you will fire salvos at a 100 \nsquare grid until all of the ships have been sunk. Good luck!");
               }
               else
                    done = true;
               Battleship battleship = new Battleship();
               battleship.play();
           }    
       }while (done != true);
       JOptionPane.showMessageDialog(null, "Thanks for playing!");
    }
}